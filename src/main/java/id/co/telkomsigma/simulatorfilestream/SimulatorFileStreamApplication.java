package id.co.telkomsigma.simulatorfilestream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class SimulatorFileStreamApplication {

    protected final Log LOGGER = LogFactory.getLog(getClass());
    private static ServerSocket server;
    //socket server port on which it will listen
    private static int port = 9010;

    public static void main(String[] args) {

        BufferedReader reader = null;
        Path path = Paths.get("/home/idxsoa/idx-mis-simul/DF26Juni2020/2020-06-26_16-06-54-vendor1.vendor");
		final List<String> data = new ArrayList<String>();
        //SpringApplication.run(SimulatorFileStreamApplication.class, args);
        try {
            server = new ServerSocket(port);
            //keep listens indefinitely until receives 'exit' call or program terminates
            while (true) {
                System.out.println("Waiting for the client request");
                //creating socket and waiting for client connection
                Socket socket = server.accept();
                System.out.println("Client Connected");
                //read from socket to ObjectInputStream object
                BufferedReader ois = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                //convert ObjectInputStream object to String
                String message;
                while ((message = ois.readLine()) != null) {
                    System.out.println("Message Received: " + message);
                }
                String[] lastRow = message.split("\\|");

                reader = Files.newBufferedReader(path);
                String line = reader.readLine();
                while (line != null) {
                    // read next line
                    data.add(line);
                    line = reader.readLine();
                }
                System.out.println("Count Data: "+data.size());
                reader.close();
                //create ObjectOutputStream object
                PrintWriter oos = new PrintWriter(socket.getOutputStream());
                if("A".equals(lastRow[2])){
                    for(int i = 0 ; i<data.size();i++){
                        //write object to Socket
                        System.out.println(data.get(i));
                        if(i / 10 == 1 || i / 10 ==10){
                            System.out.println("Hold Process");
                            TimeUnit.MINUTES.sleep(10);
                        }
                        oos.write(data.get(i));
                    }
                }else{
                    int lastNumber = Integer.parseInt(lastRow[2]);
                    System.out.println("lastNumber: " + lastNumber);
                    for(int i = lastNumber ; i<data.size();i++){
                        //write object to Socket
                        System.out.println(data.get(i));
                        if(i / 10 == 1 || i / 10 ==10){
                            System.out.println("Hold Process");
                            TimeUnit.MINUTES.sleep(10);
                        }
                        oos.write(data.get(i));
                    }
                }
                oos.close();
                //close resources
				ois.close();
				socket.close();
                //terminate the server if client sends exit request
                if (message.equalsIgnoreCase("exit")) break;
            }
            System.out.println("Shutting down Socket server!!");
            //close the ServerSocket object
            server.close();

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }

}
