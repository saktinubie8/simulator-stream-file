/*
package id.co.telkomsigma.simulatorfilestream.controller;

import id.co.telkomsigma.simulatorfilestream.pojo.UploadFileResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api")
public class UploadFileController {

    @Value("${destination.host}")
    private String destinationHost;

    @Value("${destination.port}")
    private int destinationPort;

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) throws IOException {

        UploadFileResponse response = new UploadFileResponse();
        BufferedReader br = null;
        String line = "";
        final List<String> data = new ArrayList<String>();
        File streamFile = new File("/home/idxsoa/idx-mis-simul/simul-streamFile/data.text");
        LOGGER.info("Start Send File to " + destinationHost + " : " + destinationPort);
        FileWriter writer = new FileWriter(streamFile);
        try {
            // send file
            br = new BufferedReader(new InputStreamReader(file.getInputStream(), "UTF-8"));
            while ((line = br.readLine()) != null) {
                if (line.equals(""))
                    continue;
                data.add(line);
                LOGGER.info("Data Number : " + data.size() + " value : " + line);
                writer.write(line);
                if (data.size() / 10 == 1 || data.size() / 10 == 10) {
                    Socket sock = new Socket(destinationHost, destinationPort);
                    byte[] mybytearray = new byte[1024];
                    InputStream is = sock.getInputStream();
                    FileOutputStream fos = new FileOutputStream(streamFile);
                    BufferedOutputStream bos = new BufferedOutputStream(fos);
                    int bytesRead = is.read(mybytearray, 0, mybytearray.length);
                    bos.write(mybytearray, 0, bytesRead);
                    bos.close();
                    sock.close();
                    TimeUnit.MINUTES.sleep(10);
                    LOGGER.info("Send File Finished");
                }
            }
            writer.close();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info("Upload File Done.");
        response.setResponseCode("SUCCESS");
        return response;

    }
}*/
