package id.co.telkomsigma.simulatorfilestream.pojo;

public class UploadFileResponse {

    private String responseCode;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
}
